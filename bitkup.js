var axios = require('axios');
var FormData = require('form-data');

API_HOST = 'https://api.bitkub.com'
API_KEY = '8152577197330eb3f8e7e69185471d48'
API_SECRET = '2cda653b648bdffcc4f00b6cd9395783'

header = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'X-BTX-APIKEY': API_KEY
}

exports.symbols = async function () {
    return new Promise(function (resolve) {
        var config = {
            method: 'get',
            url: API_HOST + '/api/market/symbols',
            headers: header
        };

        axios(config).then(function (response) {
            // if (response.data.status == 200) {
            //     resolve({ code: 200 })
            // } else {
            //     resolve({ code: 400 })
            // }

            if (response.data.error == 0) {
                resolve({
                    code: 200,
                    result: response.data.result
                })
            } else {
                resolve({
                    code: 400
                })
            }
            
        }).catch(function (error) {
            console.log(error);
            resolve({ code: 400 })
        });
    })
}