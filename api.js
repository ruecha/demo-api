let express = require('express');
let app = express();
let bodyParser = require('body-parser');
const port = 3005;
const LineNotify = require('./lineNotify')
const MongoClient = require('mongodb').MongoClient;
const DBHandle = require('./db/DBHandle');
const init_db = require('./init_db.json')
const functionPackage = require('./functionPackage')
const { io } = require("socket.io-client");
const moment = require('moment-timezone')
const ObjectId = require('mongodb').ObjectId;
const cors = require('cors')({
    origin: true
})

app.use(cors);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const MongoClient_url = 'mongodb://' + init_db.db_admin.username + ':' + init_db.db_admin.password + '@' + init_db.db_admin.ip + ":" + init_db.db_admin.port + '/?authSource=' + init_db.db_admin.auth_source;
var Database
DBHandle.ConnectDB(MongoClient, MongoClient_url).then(async function (result) {
    Database = result.db(init_db.db_admin.db_name)
    app.listen(port, () => {
        console.log('Node App is running on port ' + port)
    })
}).catch(function (error) {
    console.log(error);
    LineNotify.sendLineNotify("ConectDB =>" + error)
})
// start api



app.post('/api/v1/register', async function (req, res) {
    var obj = req.body
    if (!obj.pname) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'pname is require' });
    } else if (!obj.fname) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'fname is require' });
    } else if (!obj.lname) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'lname is require' });
    } else if (!obj.phone_number) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'phone_number is require' });
    } else if (obj.phone_number.length != 10) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'invalid phone number' });
    } else if (!obj.password) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'password is require' });
    } else if (obj.password.length < 6) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'password must contain 6 characters or more' });
    } else if (!obj.confirm_password) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'confirm_password is require' });
    } else if (obj.password != obj.confirm_password) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'passwords do not match' });
    } else {
        var user = await DBHandle.queryOne(Database, 'user_data', { phone_number: obj.phone_number });
        if (user) {
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ "code": 409, "message": "The phone number has been registered" });
        } else {

            var raw = {
                auth: {
                    access_token: functionPackage.encrypt(JSON.stringify({
                        pname: obj.pname,
                        fname: obj.fname,
                        lname: obj.lname,
                        random_string: functionPackage.generateUUID()
                    })),
                    password: functionPackage.encrypt(obj.password)
                },
                pname: obj.pname,
                fname: obj.fname,
                lname: obj.lname,
                phone_number: obj.phone_number,
                email: obj.email,
                create_at: Number(moment.tz('Asia/Bangkok').format('x'))
            }
            await DBHandle.putCollection(Database, 'user_data', raw)
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({
                code: 200,
                message: 'register success'
            });
        }
    }
})
app.post('/api/v1/login', async function (req, res) {
    var obj = req.body
    if (!obj.phone_number) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'phone_number is require' });
    } else if (!obj.password) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'password is require' });
    } else {
        var user = await DBHandle.queryOne(Database, 'user_data', { phone_number: obj.phone_number });
        if (user) {

            if (functionPackage.decrypt(user.auth.password) == obj.password) {
                var access_token = functionPackage.encrypt(JSON.stringify({
                    pname: user.pname,
                    fname: user.fname,
                    lname: user.lname,
                    random_string: functionPackage.generateUUID()
                }));
                await DBHandle.updateOne(Database, 'user_data', { _id: user._id }, { $set: { 'auth.access_token': access_token } })

                user.auth.access_token = access_token

                delete user.auth.password
                delete user.create_at

                res.status(200);
                res.set('Content-Type', 'application/json');
                res.json({
                    'code': 200,
                    'message': 'success',
                    'user_data': user
                });


            } else {
                res.status(200);
                res.set('Content-Type', 'application/json');
                res.json({ 'code': 401, 'message': 'incorrect password' });
            }
        } else {
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ 'code': 401, 'message': 'phone number not found' });
        }
    }
})
app.post('/api/v1/getUserData', async function (req, res) {
    functionPackage.checkAccessToken(Database, req.headers.authorization).then(async function (result) {
        if (result.isVerify == true) {
            delete result.isVerify;
            delete result.create_at;
            delete result.auth;
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({
                "code": 200,
                "message": "Success",
                "user_data": result
            });
        } else {
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ "code": 401, "message": "Unauthorized" });
        }
    }).catch(function (error) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ "code": 401, "message": "Unauthorized." });
    })
})
// wallet
app.post('/api/v1/getWalletImageList', async function (req, res) {
    functionPackage.checkAccessToken(Database, req.headers.authorization).then(async function (result) {
        if (result.isVerify == true) {
            let wallet_image_list = await DBHandle.queryListSort(Database, 'wallet_image', {}, 1)
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({
                "code": 200,
                "message": "success",
                "wallet_image_list": wallet_image_list
            });
        } else {
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ "code": 401, "message": "Unauthorized" });
        }
    }).catch(function (error) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ "code": 401, "message": "Unauthorized." });
    })
})
app.post('/api/v1/addWallet', async function (req, res) {
    var obj = req.body
    functionPackage.checkAccessToken(Database, req.headers.authorization).then(async function (result) {
        if (result.isVerify == true) {
            if (!obj.wallet_name) {
                res.status(200);
                res.set('Content-Type', 'application/json');
                res.json({ 'code': 401, 'message': 'wallet_name is require' });
            } else if (!obj.wallet_image) {
                res.status(200);
                res.set('Content-Type', 'application/json');
                res.json({ 'code': 401, 'message': 'walelt_image is require' });
            } else {
                var wallet = await DBHandle.queryOne(Database, 'wallet', { "user_data.user_id": result._id, "wallet_name": obj.wallet_name });
                if (wallet) {
                    res.status(200);
                    res.set('Content-Type', 'application/json');
                    res.json({ "code": 409, "message": "There is already a wallet with this name." });
                } else {
                    var raw = {
                        "user_data": {
                            "user_id": result._id.toString(),
                            "pname": result.pname,
                            "fname": result.fname,
                            "lname": result.lname
                        },
                        "wallet_name": obj.wallet_name,
                        "wallet_image": obj.wallet_image,
                        "timestamp": Number(moment.tz('Asia/Bangkok').format('x'))
                    }
                    await DBHandle.putCollection(Database, 'wallet', raw)
                    res.status(200);
                    res.set('Content-Type', 'application/json');
                    res.json({
                        "code": 200,
                        "message": "Success"
                    });
                }
            }
        } else {
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ "code": 401, "message": "Unauthorized" });
        }
    }).catch(function (error) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ "code": 401, "message": "Unauthorized." });
    })
})
app.post('/api/v1/removeWallet', async function (req, res) {
    var obj = req.body
    functionPackage.checkAccessToken(Database, req.headers.authorization).then(async function (result) {
        if (result.isVerify == true) {
            if (!obj.wallet_id) {
                res.status(200);
                res.set('Content-Type', 'application/json');
                res.json({ 'code': 401, 'message': 'wallet_id is require' });
            } else {
                var wallet = await DBHandle.queryOne(Database, 'wallet', { "user_data.user_id": result._id.toString(), "_id": ObjectId(obj.wallet_id) });
                if (wallet) {
                    await DBHandle.deleteOne(Database, 'wallet', { "_id": ObjectId(obj.wallet_id) })
                    res.status(200);
                    res.set('Content-Type', 'application/json');
                    res.json({ "code": 200, "message": "success" });
                } else {
                    res.status(200);
                    res.set('Content-Type', 'application/json');
                    res.json({ "code": 409, "message": "wallet_id not found" });
                }
            }
        } else {
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ "code": 401, "message": "Unauthorized" });
        }
    }).catch(function (error) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ "code": 401, "message": "Unauthorized." });
    })
})
app.post('/api/v1/getWalletList', async function (req, res) {
    functionPackage.checkAccessToken(Database, req.headers.authorization).then(async function (result) {
        if (result.isVerify == true) {
            var wallet_list = await DBHandle.queryListSort(Database, 'wallet', { "user_data.user_id": result._id.toString() }, 1)
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({
                "code": 200,
                "message": "success",
                "wallet_list": wallet_list
            });
        } else {
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ "code": 401, "message": "Unauthorized" });
        }
    }).catch(function (error) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ "code": 401, "message": "Unauthorized." });
    })
})
// income and expenses history
app.post('/api/v1/addListOfIncodeAndExpenses', async function (req, res) {
    var obj = req.body
    functionPackage.checkAccessToken(Database, req.headers.authorization).then(async function (result) {
        if (result.isVerify == true) {
            if (!obj.wallet_id) {
                res.status(200);
                res.set('Content-Type', 'application/json');
                res.json({ 'code': 401, 'message': 'wallet_id is require' });
            } else if (!obj.type) {
                res.status(200);
                res.set('Content-Type', 'application/json');
                res.json({ 'code': 401, 'message': 'type is require' });
            } else if (!['income', 'expenses'].includes(obj.type)) {
                res.status(200);
                res.set('Content-Type', 'application/json');
                res.json({ 'code': 401, 'message': 'type is invalid' });
            } else if (!obj.amount) {
                res.status(200);
                res.set('Content-Type', 'application/json');
                res.json({ 'code': 401, 'message': 'amount is require' });
            } else if (!obj.category) {
                res.status(200);
                res.set('Content-Type', 'application/json');
                res.json({ 'code': 401, 'message': 'category is require' });
            } else {
                var category = await DBHandle.queryOne(Database, 'category', { "category": obj.category })
                if (category) {
                    var raw = {
                        "wallet_id": obj.wallet_id,
                        "type": obj.type,
                        "amount": Number(obj.amount),
                        "category": obj.category,
                        "detail": obj.detail,
                        "date": moment.tz("Asia/Bangkok").format('YYYY-MM-DD'),
                        "time": moment.tz("Asia/Bangkok").format('HH:mm'),
                        "timestamp": Number(moment.tz('Asia/Bangkok').format('x'))
                    }
                    await DBHandle.putCollection(Database, 'income_and_expenses_history', raw)
                    res.status(200);
                    res.set('Content-Type', 'application/json');
                    res.json({ 'code': 200, 'message': 'success' });
                } else {
                    res.status(200);
                    res.set('Content-Type', 'application/json');
                    res.json({ "code": 409, "message": "category not found" });
                }
            }
        } else {
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ "code": 401, "message": "Unauthorized" });
        }
    }).catch(function (error) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ "code": 401, "message": "Unauthorized." });
    })
})
app.post('/api/v1/getHostoryIncodeAndExpenses', async function (req, res) {
    var obj = req.body
    functionPackage.checkAccessToken(Database, req.headers.authorization).then(async function (result) {
        if (result.isVerify == true) {
            if (!obj.wallet_id) {
                res.status(200);
                res.set('Content-Type', 'application/json');
                res.json({ 'code': 401, 'message': 'wallet_id is require' });
            } else {
                var history_list = await DBHandle.queryListSort(Database, 'income_and_expenses_history', { 'wallet_id': obj.wallet_id }, 1)
                res.status(200);
                res.set('Content-Type', 'application/json');
                res.json({
                    "code": 200,
                    "message": "success",
                    "history_list": history_list
                });
            }
        } else {
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ "code": 401, "message": "Unauthorized" });
        }
    }).catch(function (error) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ "code": 401, "message": "Unauthorized." });
    })
})
// end api
module.exports = app;