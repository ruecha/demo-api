let express = require('express');
let app = express();
let bodyParser = require('body-parser');
const fs = require('fs');
let mysql = require('mysql');
const { v4: uuid } = require('uuid')
const port = 3000;
const http = require('http')
const server = http.createServer(app)
const { Server } = require('socket.io')
const io = new Server(server)
const LineNotify = require('./lineNotify')
const Bitkup = require('./bitkup')
const MongoClient = require('mongodb').MongoClient;
const DBHandle = require('./db/DBHandle');
const init_db = require('./init_db.json')

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const MongoClient_url = 'mongodb://' + init_db.db_admin.username + ':' + init_db.db_admin.password + '@' + init_db.db_admin.ip + ":" + init_db.db_admin.port + '/?authSource=' + init_db.db_admin.auth_source;
var TON_DB
DBHandle.ConnectDB(MongoClient, MongoClient_url).then(async function (result) {
    TON_DB = result.db(init_db.db_admin.db_name)

    app.listen(port, () => {
        console.log('Node App is running on port 3000')
    })
}).catch(function (error) {
    console.log(error);
    LineNotify.sendLineNotify("ConectDB =>" + error)
})



app.post('/api/v1/demo/sendLineNotify', async function (req, res) {
    var obj = req.body

    if (!obj.message) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'message is require' });
    } else {
        var response = await LineNotify.sendLineNotify(obj.message)
        if (response.code == 200) {
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ 'code': 200, 'message': 'success' });
        } else {
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ 'code': 400, 'message': 'invalid' });
        }
    }
})

app.post('/api/v1/demo/testBitkup', async function (req, res) {
    var obj = req.body

    var result = await Bitkup.symbols()

    res.status(200);
    res.set('Content-Type', 'application/json');
    res.json({ 'code': 200, 'message': 'success', 'result': result });
})

app.post('/api/v1/ton/addUserData', async function (req, res) {
    var obj = req.body
    if (!obj.pname) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'pname is require' });
    } else if (!obj.fname) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'fname is require' });
    } else if (!obj.lname) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'lname is require' });
    } else {
        var val = {
            pname: obj.pname,
            fname: obj.fname,
            lname: obj.lname
        }
        await DBHandle.putCollection(TON_DB, 'user_data', val)
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 200, 'message': 'Succeess' });
    }
})
app.post('/api/v1/ton/getUserDataList', async function (req, res) {
    var userDataList = await DBHandle.queryList(TON_DB, 'user_data')
    res.status(200);
    res.set('Content-Type', 'application/json');
    res.json({ 'code': 200, 'message': 'Succeess', 'user_data_list': userDataList });
})

module.exports = app;
