let express = require('express');
let app = express();
let bodyParser = require('body-parser');
const fs = require('fs');
const { v4: uuid } = require('uuid')
const port = 3000;
const http = require('http')
const server = http.createServer(app)
const { Server } = require('socket.io')
const io = new Server(server)
const LineNotify = require('./lineNotify')
const Bitkup = require('./bitkup')
const cron = require('node-cron');

finishWork()

async function finishWork() {
	cron.schedule('30 16 * * *', async () => {
        await LineNotify.sendLineNotify("เลิกงานแล้ว")
	}, {
		scheduled: true,
		timezone: "Asia/Bangkok"
	});
}