
const DBHandle = require('./db/DBHandle');
const moment = require('moment-timezone')

exports.sendMessage = async function (Database, chat_room_id, sender_name, receiver_name, message) {
    let raw = {
        chat_room_id: chat_room_id,
        sender_name: sender_name,
        receiver_name: receiver_name,
        message: message,
        read: false,
        timestamp : Number(moment.tz('Asia/Bangkok').format('x'))

    }
    await DBHandle.putCollection(Database, 'message', raw)
    return {
        message: "success"
    }
}
exports.updateLastMessage = async function (Database, chat_room_id, sender_name, receiver_name, message) {
    let raw = {
        sender_name: sender_name,
        receiver_name: receiver_name,
        last_message: message,
    }
    await DBHandle.updateOne(Database, 'chat_room', {room_id: chat_room_id}, {$set: raw})
    return {
        message: "success"
    }
}
