const express = require('express');
const socketio = require('socket.io')
const app = express();

const port = 3007
app.set('view engine', 'ejs')
app.use(express.static('public'))

app.get('/', (req, res) => {
    res.json({ "message": "server is running" })
});

const cors = require('cors')({
    origin: true
})

app.use(cors);

const server = app.listen(port, () => {
    console.log("running on port " + port);
})

const io = socketio(server)

io.on('connection', (socket) => {
    console.log("new user connected");

    socket.on("send_message", async data => {
        console.log("send_message")
        console.log(data);
        io.sockets.emit("save_message_to_database", data)
        // await Service.sendMessage(data['chat_room_id'], data['sender_name'], data['receiver_name'], data['message'])
        socket.broadcast.emit('receive_message', data)
    })

})


