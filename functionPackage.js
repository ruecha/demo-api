const crypto = require('crypto')
const sha256 = require('sha256')
const { v4: uuidv4 } = require('uuid')
const { v5: uuidv5 } = require('uuid')
const DBHandle = require('./db/DBHandle');


const password_token = "54c748b7-eb6b-4f20-831c-1f165f215072"


exports.generateUUID = function () {
    return uuidv5(uuidv4(), uuidv4());
}

exports.gen_iv = function () {
    var text = "";
    var possible = "=/+-abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";

    for (var i = 0; i < 16; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}

exports.encrypt = function (data) {
    try {
        var IV = this.gen_iv()
        var binaryEncryptionKey = crypto.createHash('sha256').update(password_token, 'utf8').digest()

        var cipher = crypto.createCipheriv("AES-256-CBC", binaryEncryptionKey, IV);

        // When encrypting, we're converting the UTF-8 input to base64 output.

        var encryptedInput = (
            cipher.update(data, 'utf8', 'base64') +
            cipher.final('base64')
        );

        return IV + encryptedInput;
    }
    catch (err) {
        return err;
    }
}

exports.decrypt = function (data) {
    try {
        var IV = data.slice(0, 16);
        var encryptedInput = data.slice(16);
        var binaryEncryptionKey = crypto.createHash('sha256').update(password_token, 'utf8').digest()

        var decipher = crypto.createDecipheriv("AES-256-CBC", binaryEncryptionKey, IV);
        var decryptedInput = (
            decipher.update(encryptedInput, 'base64', 'utf8') +
            decipher.final('utf8')
        );

        return decryptedInput;
    }
    catch (err) {
        return err;
    }

}

exports.checkAccessToken = async function (Database, access_token) {
	
	var raw = {
		isVerify : false
	}
	
	return new Promise(function (resolve, reject) {
	DBHandle.queryOne(Database, 'user_data', { 'auth.access_token' : access_token}).then(function(result)
				{
					if(result)
					{
						result.isVerify = true;
						
						resolve(result);
					}
					else
					{
						resolve(raw);
					}
				})
				.catch(function(error){
					console.log(error);
					resolve(raw);
				});
	});
}
