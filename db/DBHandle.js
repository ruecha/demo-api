exports.ConnectDB = function (MongoClient, MongoClient_url) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient.connect(MongoClient_url, { useNewUrlParser: true, useUnifiedTopology: true }, function(err, db) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				
				resolve(db);
			}
		
		});

	});

};

exports.saveMongoDBOne = function (MongoClient_DB, collection, raw) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).insertOne(raw, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				console.log("1 document inserted");
				resolve("1 document inserted");
			}

		});
	  
	});

};

exports.saveMongoDBMany = function (MongoClient_DB, collection, raw) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).insertMany(raw, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				console.log("Number of documents inserted: " + res.insertedCount);
				resolve("Number of documents inserted: " + res.insertedCount);
			}
		});
	  
	});

};


exports.dropCollection = function (MongoClient_DB, collection) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.listCollections().toArray(function(err, items){
			if (err) throw err;

			var exists = false;
			items.forEach(function(childSnapshot) {
				console.log('Collection : '+childSnapshot.name);
				
				if(childSnapshot.name == collection)
				{
					exists = true;
				}
			});
			
			if(exists)
			{
				MongoClient_DB.collection(collection).drop(function(err, delOK) {
					if (err) 
					{
						//throw err;
						reject(err);
					}
					else
					{
						
						resolve("Collection : "+collection+" deleted");
					}

				});
			}
			else
			{
				resolve("Collection : "+collection+" deleted");
			}
	
		  });  
		
	  
	});

};

exports.saveAccessTokenAgent = function (MongoClient_DB, collection, raw) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).insertOne(raw, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve("1 access token inserted");
			}

		});
	  
	});

};

exports.getAccessTokenAgent = function (MongoClient_DB, access_token) {
	
	var query = { access_token : access_token};
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection('agent').findOne(query, function(err, result) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(result);
			}

		});
	  
	});

};


exports.getPersonalInformationByPID = function (MongoClient_DB, pid) {
	
	var query = { "search.pid" : pid};
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection("personal_information").find(query).toArray(function(err, result) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(result);
			}
		});
	  
	});

};

exports.getInformationByPid = function (MongoClient_DB, collection, pid) {
	
	var query = { "search.pid" : pid};
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).find(query).toArray(function(err, result) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(result);
			}
		});
	  
	});

};

exports.getInformationByPidAttachSearch = function (MongoClient_DB, collection, attach_search) {
	
	var query = { "search.attach_search" : attach_search};
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).find(query).toArray(function(err, result) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(result);
			}
		});
	  
	});

};

exports.getInformationByCheckExists = function (MongoClient_DB, collection, check_exists) {
	
	var query = { "search.check_exists" : check_exists};
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).find(query).toArray(function(err, result) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(result);
			}
		});
	  
	});

};

exports.updateInformationByCheckExists = function (MongoClient_DB, collection, raw) {
	
	var query = { "search.check_exists" : raw.search.check_exists};
	var newvalues = { $set: raw };
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).replaceOne(query, raw, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve('1 document updated');
			}
		});
	  
	});

};

exports.putInformation = function (MongoClient_DB, collection, raw) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).insertOne(raw, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				// console.log("1 document inserted");
				resolve("1 document inserted");
			}

		});
	  
	});

};

//////////////////////////////////////////////////////////////////////////////////////

exports.CheckAccessTokenUserExists = function (MongoClient_DB, access_token) {
	
	var query = { "search.access_token" : access_token};
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection('user_data').findOne(query, function(err, result) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(result);
			}

		});
	  
	});

};

exports.CheckAccessTokenAgentExists = function (MongoClient_DB, access_token) {
	
	var query = { "search.access_token" : access_token};
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection('agent').find(query).toArray(function(err, result) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(result);
			}
		});
	  
	});

};

exports.CheckAccessTokenProvincePrivateServerExists = function (MongoClient_DB, province_id) {
	
	var query = { "province_id" : province_id};
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection('province').findOne(query, function(err, result) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(result);
			}
		});
	  
	});

};

exports.getAccessTokenUserExists = function (MongoClient_DB, uid) {
	
	var query = { "search.uid" : uid};
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection('user_data').findOne(query, function(err, result) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(result);
			}

		});
	  
	});

};

exports.getAccessTokenAsmExists = function (MongoClient_DB, uid) {
	
	var query = { "search.uid" : uid};
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection('user_data_asm').findOne(query, function(err, result) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(result);
			}

		});
	  
	});

};

exports.removeAccessTokenUserExists = function (MongoClient_DB, uid) {
	
	var query = { "search.uid" : uid};
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection('user_data').deleteOne(query, function(err, result) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(result);
			}

		});
	  
	});

};

exports.getAccessTokenAgentByHosId = function (MongoClient_DB, hos_id) {
	
	var query = { "search.check_exists" : hos_id};
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection('agent').findOne(query, function(err, result) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(result);
			}

		});
	  
	});

};

exports.removeAccessTokenAgentByHosId = function (MongoClient_DB, hos_id) {
	
	var query = { "search.check_exists" : hos_id};
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection('agent').deleteOne(query, function(err, result) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(result);
			}

		});
	  
	});

};

exports.updateCollectionByCheckExists = function (MongoClient_DB, collection, raw) {
	
	var query = { "search.check_exists" : raw.search.check_exists};
	var newvalues = { $set: raw };
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).replaceOne(query, raw, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				console.log("1 document updated");
				resolve('1 document updated');
			}
		});
	  
	});

};

exports.putCollection = function (MongoClient_DB, collection, raw) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).insertOne(raw, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				// console.log("1 document inserted");
				resolve("1 document inserted");
			}

		});
	  
	});

};

exports.putCollectionList = function (MongoClient_DB, collection, raw) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).insertMany(raw, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				console.log("Number of documents inserted: " + res.insertedCount);
				resolve("Number of documents inserted: " + res.insertedCount);
			}
		});
	  
	});

};

exports.queryOne = function (MongoClient_DB, collection, query) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).findOne(query, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	  
	});

};

exports.queryList = function (MongoClient_DB, collection, query) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).find(query).toArray(function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	  
	});

};

exports.queryListSort = function (MongoClient_DB, collection, query, sort) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).find(query).sort( { timestamp: sort } ).toArray(function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	  
	});

};

exports.queryListProjection = function (MongoClient_DB, collection, query, projection_) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).find(query, {projection : projection_}).toArray(function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	  
	});

};

exports.queryListWithLimitTolast = function (MongoClient_DB, collection, query, limit) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).find(query).sort({ _id: -1 }).limit(limit).toArray(function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	  
	});

};

exports.queryListWithLimitTofirst = function (MongoClient_DB, collection, query, limit) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).find(query).limit(limit).toArray(function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	  
	});

};

exports.queryJoinList = function (MongoClient_DB, collection_local,collection_foreign, key_local, key_foreign, as) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection_local).aggregate([
			{ $lookup:
			   {
				 from: collection_foreign,
				 localField: key_local,
				 foreignField: key_foreign,
				 as: as
			   }
			 }
			]).toArray(function(err, res) {
				if (err) 
				{
					throw err;
					reject(err);
				}
				else
				{
					resolve(res);
				}

		});
	  
	});

};

exports.queryJoinListWithQuery = function (MongoClient_DB, collection_local,collection_foreign, key_local, key_foreign, query, as) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection_local).aggregate([
			{ $lookup:
			   {
				 from: collection_foreign,
				 localField: key_local,
				 foreignField: key_foreign,
				 as: as
			   }
			 },
			  { $match : query }
			]).toArray(function(err, res) {
				if (err) 
				{
					throw err;
					reject(err);
				}
				else
				{
					resolve(res);
				}

		});
	  
	});

};

exports.updateOne = function (MongoClient_DB, collection, query, raw) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).updateOne(query, raw, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	  
	});

};

exports.updateMany = function (MongoClient_DB, collection, query, raw) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).updateMany(query, raw, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	  
	});

};

exports.findOneAndUpdate = function (MongoClient_DB, collection, query, raw){

	return new Promise(function (resolve, reject) {
	
	   MongoClient_DB.collection(collection).findOneAndUpdate(
		  query,
		  raw,
		  { upsert: true, returnOriginal: false}
	   , function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	   
   });
}

exports.findOneAndUpdateWithFilter = function (MongoClient_DB, collection, query, raw, filter){

	return new Promise(function (resolve, reject) {
	
	   MongoClient_DB.collection(collection).findOneAndUpdate(
		  query,
		  raw,
		  { multi: true, arrayFilters: [filter], returnOriginal: false }
	   , function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	   
   });
}

exports.updateInsertOne = function (MongoClient_DB, collection, query, raw) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).updateOne(query, raw, { upsert: true }, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	  
	});

};

exports.updateWhenNotExists = function (MongoClient_DB, collection, query, raw) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).updateOne(query, raw, { multi: true }, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	  
	});

};

exports.updateInsertWhenNotExists = function (MongoClient_DB, collection, query, raw) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).updateOne(query, raw, { multi: true }, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	  
	});

};

exports.replaceInsertOne = function (MongoClient_DB, collection, query, raw) {
	
	console.log('call : replaceInsertOne');
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).replaceOne(query, raw, { upsert: true }, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	  
	});

};

exports.deleteOne = function (MongoClient_DB, collection, query) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).deleteOne(query, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	  
	});

};

exports.deleteMany = function (MongoClient_DB, collection, query) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).deleteMany(query, function(err, res) {
			if (err) 
			{
				throw err;
				reject(err);
			}
			else
			{
				resolve(res);
			}

		});
	  
	});

};


exports.queryCountGroup = function (MongoClient_DB, collection, key) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).aggregate([
			{ $group:
			   {
				  _id: key,
				  count: { $sum: 1 }
				}
			 }
			]).toArray(function(err, res) {
				if (err) 
				{
					throw err;
					reject(err);
				}
				else
				{
					resolve(res);
				}

		});
	  
	});

};

exports.queryGroup = function (MongoClient_DB, collection, key) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).aggregate([
			{ $group:
			   {
				  _id: key,
				  group_list: { $addToSet: '$'+key }
				}
			 }
			]).toArray(function(err, res) {
				if (err) 
				{
					throw err;
					reject(err);
				}
				else
				{
					resolve(res);
				}

		});
	  
	});

};

exports.queryAggregate = function (MongoClient_DB, collection, query) {
	
	return new Promise(function (resolve, reject) {
		
		MongoClient_DB.collection(collection).aggregate(query, { allowDiskUse: true }).toArray(function(err, res) {
				if (err) 
				{
					throw err;
					reject(err);
				}
				else
				{
					resolve(res);
				}

		});
	  
	});

};


