let express = require('express');
let app = express();
let bodyParser = require('body-parser');
const port = 3008;
const LineNotify = require('./lineNotify')
const MongoClient = require('mongodb').MongoClient;
const DBHandle = require('./db/DBHandle');
const init_db = require('./init_db.json')
const functionPackage = require('./functionPackage')
const { io } = require("socket.io-client");
const moment = require('moment-timezone')
const ObjectId = require('mongodb').ObjectId;
const cors = require('cors')({
    origin: true
})
const service = require('./service')

app.use(cors);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
const MongoClient_url = 'mongodb://' + init_db.db_admin.username + ':' + init_db.db_admin.password + '@' + init_db.db_admin.ip + ":" + init_db.db_admin.port + '/?authSource=' + init_db.db_admin.auth_source;
var Database
var socket
DBHandle.ConnectDB(MongoClient, MongoClient_url).then(async function (result) {
    Database = result.db('Chat')

    socket = io("https://ton.ballboo088.com")

    socket.on("save_message_to_database", async function(data) {
        await service.sendMessage(Database, data['chat_room_id'], data['sender_name'], data['receiver_name'], data['message'])
        await service.updateLastMessage(Database, data['chat_room_id'], data['sender_name'], data['receiver_name'], data['message'])
    })


    app.listen(port, () => {
        console.log('Node App is running on port ' + port)
    })
}).catch(function (error) {
    console.log(error);
    LineNotify.sendLineNotify("ConectDB =>" + error)
})
// start api

app.post('/api/v1/chat/login', async function (req, res) {
    let obj = req.body
    if (!obj.user_name) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'user_name is require' });
    } else {
        let user_data = await DBHandle.queryOne(Database, 'user_data', { user_name: obj.user_name })
        if (user_data) {
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ 'code': 200, 'message': 'success', "user_data": user_data });
        } else {
            await DBHandle.putCollection(Database, 'user_data', { user_name: obj.user_name })
            let result = await DBHandle.queryOne(Database, 'user_data', { user_name: obj.user_name })
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ 'code': 200, 'message': 'success', "user_data": result });
        }
    }
})

app.post('/api/v1/chat/getUserList', async function (req, res) {
    let user_list = await DBHandle.queryList(Database, 'user_data', {})
    res.status(200);
    res.set('Content-Type', 'application/json');
    res.json({ 'code': 200, 'message': 'success', "user_list": user_list });
})

app.post('/api/v1/chat/getChatRoomList', async function (req, res) {
    let obj = req.body

    if (!obj.user_name) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'user_name is require' });
    } else {
        let chat_room_list_all = await DBHandle.queryList(Database, 'chat_room', {})
        let chat_room_list = []
        for await (let room of chat_room_list_all) {
            if (room['user1'] == obj.user_name || room['user2'] == obj.user_name) {
                chat_room_list.push(room)
            }
        }

        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 200, 'message': 'success', "chat_room_list": chat_room_list });
    }
})

app.post('/api/v1/chat/createChatRoom', async function (req, res) {
    let obj = req.body
    if (!obj.user_id) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'user_id is require' });
    } else if (!obj.user_name) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'user_name is require' });
    } else if (!obj.receiver_id) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'receiver_id is require' });
    } else if (!obj.receiver_name) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'receiver_name is require' });
    } else {
        let chat_room_list = await DBHandle.queryList(Database, 'chat_room', {})
        let haveRoom = false
        for await (let chat_room of chat_room_list) {
            if (chat_room.room_id.includes(obj.user_id) && chat_room.room_id.includes(obj.receiver_id) && (chat_room.room_id == (obj.user_id + obj.receiver_id) || chat_room.room_id == (obj.receiver_id + obj.user_id))) {
                haveRoom = true
                res.status(200);
                res.set('Content-Type', 'application/json');
                res.json({ 'code': 200, 'message': 'success', 'chat_room': chat_room });
            }
        }

        if (haveRoom) {
            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ 'code': 401, 'message': 'error' });
        } else {
            await DBHandle.putCollection(Database, 'chat_room', {
                "room_id": obj.user_id + obj.receiver_id,
                "user1": obj.user_name,
                "user2": obj.receiver_name
            })

            res.status(200);
            res.set('Content-Type', 'application/json');
            res.json({ 'code': 200, 'message': 'success', 'chat_room': {
                "room_id": obj.user_id + obj.receiver_id,
                "user1": obj.user_name,
                "user2": obj.receiver_name
            } });
        }
    }
})

app.post('/api/v1/chat/getMessageList', async function (req, res) {
    let obj = req.body

    if (!obj.chat_room_id) {
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 401, 'message': 'chat_room_id is require' });
    } else {
        let message_list = await DBHandle.queryListSort(Database, 'message', { chat_room_id: obj.chat_room_id }, 1)
        res.status(200);
        res.set('Content-Type', 'application/json');
        res.json({ 'code': 200, 'message': 'success', 'message_list': message_list });
    }
})

// exports.sendMessage = async function(chat_room_id, sender_name, receiver_name, message) {
//     return new Promise(async function (resolve) {
//         let raw = {
//             chat_room_id: chat_room_id,
//             sender_name: sender_name,
//             receiver_name: receiver_name,
//             message: message,
//             read: false
//         }
//         await DBHandle.putCollection(Database, 'message', raw)
//         resolve({
//             message: 'success'
//         })
//     })
// }


// end api
module.exports = app;