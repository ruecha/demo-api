var axios = require('axios');
var FormData = require('form-data');

exports.sendLineNotify = async function (message) {
    return new Promise(function (resolve) {
        var data = new FormData();
        data.append('message', message);

        var config = {
            method: 'post',
            url: 'https://notify-api.line.me/api/notify',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer 2Oyf22jIldriIKK7f513JLmMF2lHLsJE9h4xHpT4tSx',
                ...data.getHeaders()
            },
            data: data
        };

        axios(config).then(function (response) {
            if (response.data.status == 200) {
                resolve({ code: 200 })
            } else {
                resolve({ code: 400 })
            }
            
        }).catch(function (error) {
            console.log(error);
            resolve({ code: 400 })
        });
    })
}
